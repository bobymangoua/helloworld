/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cricketmatch_test.wtech.sport;

/**
 *
 * @author lmangoua
 */
public abstract class Player {
    private String name;
    private String surname;
    
    public Player(String name, String surname) {
        //only use "this" when u wand to call a variable within the same class ur working on, 
        //but i think even with global variables u can also use it
        
        this.name = name;
        this.surname = surname;
    }
    
    public String getName() {
        return name;
    }
    
    public String getSurname() {
        return surname;
    }
    
    public abstract void Play();
}
