/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package multidimentionalarrays;

/**
 *
 * @author lmangoua
 * Date: 08/09/16
 * Source: http://www.homeandlearn.co.uk/java/multi-dimensional_arrays.html
 */
public class MultiDimentionalArrays {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        int[][] aryNumbers = new int[3][4];
        
        aryNumbers[0][0] = 10;          aryNumbers[1][0] = 13;
        aryNumbers[0][1] = 12;          aryNumbers[1][1] = 11;
        aryNumbers[0][2] = 16;          aryNumbers[1][2] = 85;
        aryNumbers[0][3] = 20;          aryNumbers[1][3] = 70;
        
        aryNumbers[2][0] = 30;          
        aryNumbers[2][1] = 15;          
        aryNumbers[2][2] = 45;          
        aryNumbers[2][3] = 90;          
        
        int rows = 3;
        int columns = 4;
        int i, j;
        
        for (i = 0; i < rows; i++) {
            for(j = 0; j < columns; j++) {
                System.out.print(aryNumbers[i][j] + " ");
            }
            System.out.println("");
        }
        
    }
    
}
