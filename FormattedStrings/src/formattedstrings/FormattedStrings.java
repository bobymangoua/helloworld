/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package formattedstrings;

/**
 *
 * @author lmangoua
 * Source: http://www.homeandlearn.co.uk/java/java_formatted_strings.html
 */
public class FormattedStrings {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        String heading1 = "Exam_Name";
        String heading2 = "Exam_Grade";
        String divider = "*********************************";
        
        String course1 = "Java";    String grade1 = "A";
        String course2 = "PHP";     String grade2 = "B";
        String course3 = ".Net";    String grade3 = "A";
        
        System.out.println("");
        System.out.printf("%-15s %15s %n", heading1, heading2);
        System.out.println(divider);
        
        System.out.printf("%-15s %10s %n", course1, grade1); //''%-15s'' means fifteen characters left-justified
        System.out.printf("%-15s %10s %n", course2, grade2); //''%10s'' means ten characters right-justified
        System.out.printf("%-15s %10s %n", course3, grade3); //To get a newline %n is used
        
        System.out.println(divider);
        System.out.println("");
        
    }
    
}
