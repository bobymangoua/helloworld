/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package filesmanagement;

import java.io.*;
import java.util.*;
import java.lang.*;

/**
 *
 * @author Lionel Mangoua
 */
public class BinaryFiles {

    // To Create File
    public static void createBinFile() {

//        //<editor-fold defaultstate="collapsed" desc="To Delete">
//        Person p = new Person();
//        p.name = "Boby";
//        p.surname = "Love";
//        p.age = 25;
//
//        File file = new File("file_%04d" + ".dat");
//        int i = 1; //increase
//
//        while (file.exists()) {
//            i++;
//            file = new File(String.format("file_%04d" + i + ".dat"));
//        }
//        if (!file.exists()) {
//            try {
//                String content;
//                content = p.toString();
//                file.createNewFile();
//
//                FileWriter fw = new FileWriter(file.getAbsoluteFile());
//                BufferedWriter bw = new BufferedWriter(fw);
//                bw.write(content);
//                bw.close();
//
//                System.out.println("Done!");
//            } catch (IOException e) {
//                // if any error occurs
//                System.out.println("You got an ERROR!");
//                e.printStackTrace();
//            }
//        }
////</editor-fold>

////<editor-fold defaultstate="collapsed" desc="To Delete 1">
//File file = new File("file_0001" + ".txt");
//int increase = 1;
//while (file.exists()) {
//    increase++;
//    file = new File("file_000" + increase + ".txt");
//}
//if (!file.exists()) {
//    try {
//        
//        String content;
//        content = p.toString();
//        file.createNewFile();
//        
//        FileWriter fw = new FileWriter(file.getAbsoluteFile());
//        BufferedWriter bw = new BufferedWriter(fw);
//        bw.write(content);
//        bw.close();
//        
//        System.out.println("Done");
//        
//    }
//    catch (IOException e) {
//        // if any error occurs
//        System.out.println("You got an ERROR!");
//        e.printStackTrace();
//    }
//}
////</editor-fold>
        File newFile;

        int index = 1;
        String name = "file_000";
        String extension = ".dat";
        while ((newFile = new File(name + index + extension)).exists()) {
            index++;
        }

    }

    // To Write File
    public static void writeBinFile() {

    }

    // To Read File
//    public static void readBinFiles() {
//
//    }
    // To Create Log File
//    public static void logFile() {
//
//    }
    //***********To Delete After, Testing Purpose***********
    //Working Code
//    //<editor-fold defaultstate="collapsed" desc="createFiles">
//    public static void createFiles() {
//        final Formatter x;
//        
//        try {
//            x = new Formatter("C:\\Users\\User\\Documents\\Programming WorkPlace\\NetBeansProjects\\GroveTest\\binary_Files\\file_%04d.dat");
//            System.out.println("File created.");
//        }
//        catch(Exception e) {
//            System.out.println("Error!");
//        }
//        
//    }
////</editor-fold>
    //Not Working Code
//    //<editor-fold defaultstate="collapsed" desc="createFiles">
//    public static void createFiles() {
////        final Formatter x;
////        
////        try {
////            x = new Formatter("file_%04d.dat");
////            System.out.println("File created.");
////        }
////        catch(Exception e) {
////            System.out.println("Error!");
////        }
//
//
//        File f;
//        String fmt = "C:\\Users\\User\\Documents\\Programming WorkPlace\\NetBeansProjects\\GroveTest\\binary_Files\\file_%04d.dat";
//        f = null;
//        
//        for (int i = 1; i < 100; i++) {
//            f = new File(String.format(fmt, i));
//            if (!f.exists()) {
//                break;
//            }
//        }
//        try {
//            
//            System.out.println(f.getCanonicalPath());
//            System.out.println("File created.");
//        }
//        catch (IOException e) {
//            System.out.println("Error!");
//            e.printStackTrace();
//        }
//
//    }
////</editor-fold>
    // To Write File(Working)
    public static void writeOnBinFiles() {
        Person p = new Person();
        p.name = "Boby";
        p.surname = "Love";
        p.age = 25;

        String fileName = "C:\\Users\\User\\Documents\\Programming WorkPlace\\NetBeansProjects\\GroveTest\\binary_Files\\file_%05d.dat";
        try {
            ObjectOutputStream os = new ObjectOutputStream(new FileOutputStream(fileName));
            os.writeObject(p);//Write Object
            os.close();//Close file
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Done writing on file.");
        System.out.println("=====================");

        //Read Object and display
        try {
            ObjectInputStream is = new ObjectInputStream(new FileInputStream(fileName));
            Person pe = (Person) is.readObject(); //read Object
            System.out.println("Read \n name = " + pe.name + "\n Surname = " + pe.surname + "\n age = " + pe.age);
            is.close();//Close file
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    // To Read File
    public static void readOnBinFiles() throws IOException {

        String fileName = "C:\\Users\\User\\Documents\\Programming WorkPlace\\NetBeansProjects\\GroveTest\\binary_Files\\file_%05d.dat";

    }

}
