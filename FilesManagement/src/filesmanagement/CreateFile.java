/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package filesmanagement;

import java.io.*;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 *
 * @author lionel Mangoua
 */
public class CreateFile {
    
    // To Create File and Write in it
    //<editor-fold defaultstate="collapsed" desc="writeOnBinFiles">
    public static void writeOnBinFiles() {
        
        Person p = new Person();
        p.name = "lionel";
        p.surname = "mangoua";
        p.age = 15;

//        String fileName = "C:\\Users\\User\\Documents\\Programming WorkPlace\\NetBeansProjects\\GroveTest\\binary_Files\\file_0001.dat";  //file_%04d.dat
        String fileName = "C:\\Users\\lmangoua\\Documents\\Lionel_Java_Projects\\FilesManagement\\binary_files\\file_0011.txt";
        try {
            ObjectOutputStream os = new ObjectOutputStream(new FileOutputStream(fileName));
            os.writeObject(p);//Write Object
            os.close();//Close file
        } 
        catch (FileNotFoundException e) {
            e.printStackTrace();
        } 
        catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("=======================");
        System.out.println("=Done writing on file.=");
        System.out.println("=======================");
        System.out.println();
        System.out.println();

        //Read Object and display
        try {
            ObjectInputStream is = new ObjectInputStream(new FileInputStream(fileName));
            Person pe = (Person) is.readObject(); //read Object
            System.out.println("Read data: \n name = " + pe.name + "\n Surname = " + pe.surname + "\n age = " + pe.age);
            System.out.println("=======================");
            System.out.println("=Done reading file.   =");
            System.out.println("=======================");
            System.out.println();
            System.out.println();
            is.close();//Close file
        } 
        catch (FileNotFoundException e) {
            e.printStackTrace();
        } 
        catch (ClassNotFoundException e) {
            e.printStackTrace();
        } 
        catch (IOException e) {
            e.printStackTrace();
        }

    }
//</editor-fold>

}
